package multitypetreeVolz.util;

import beast.core.BEASTObject;
import beast.core.Distribution;
import beast.core.Input;
import beast.core.Loggable;
import multitypetreeVolz.distributions.StructuredCoalescentTreeDensityVolz;

import java.io.PrintStream;

/**
 * Created by jessiewu on 04/10/16.
 */
public class BastaRootProbLogger extends BEASTObject implements Loggable {
    public Input<StructuredCoalescentTreeDensityVolz> distrInput = new Input<StructuredCoalescentTreeDensityVolz>(
            "distr",
            "The StructuredCoalescentTreeDensityVolz object with the root probabilities to be logged in an output.",
            Input.Validate.REQUIRED
    );

    private int typeCount;
    private StructuredCoalescentTreeDensityVolz distr;
    private Double[] rootProbs;
    public void initAndValidate(){

        distr = distrInput.get();
        typeCount = distr.getNDemes();
        rootProbs = new Double[typeCount];
    }


    @Override
    public void init(PrintStream out)  {
        for(int i = 0; i < typeCount; i++) {
            out.print(distr.getID()+"rootProb."+ i + "\t");
        }

    }

    @Override
    public void log(int nSample, PrintStream out) {
        distr.getRootProbs(rootProbs);
        for(int i = 0; i < rootProbs.length; i++){
            out.print(rootProbs[i] + "\t");
        }


    }

    @Override
    public void close(PrintStream out) {
    }
}